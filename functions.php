<?php
global $Hooks;

/*
 * Auto Callback for Hook
 *
 * Configuration
 *
 * config.php HOOK = 1
 * config.php Log = 1
 */
function hook_action_auto_callback($args) 
{
	$for =  str_replace(array('After_','Before_', '_'), array('','',' '), $args['for']);
	unset($args['for']);
	$data = isset($args[0]) ? json_encode($args[0]) : '';
	$msg = date('Y-m-d G:i:s') . ' - ' . $for . ': ' . $data . ' by ' . get_the_user(user_id(),'username');

	$handle = fopen(DIR_STORAGE.'activity-logs/'.date('Y-m-d').'.txt', 'a');
	fwrite($handle, $msg . "\n");
}